<!DOCTYPE html>
<html lang="en">
<head>
    <!-- set the encoding of your site -->
    <meta charset="utf-8">
    <!-- set the viewport width and initial-scale on mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- set the HandheldFriendly -->
    <meta name="HandheldFriendly" content="True">
    <!-- set the description -->
    <meta name="description" content="Pegasus Trans provides on time and cost efficient road transport for oversize / special transports. We specialise in road transport within the European Union, with a special focus on Germany, France and Great Britain. ">
    <!-- set the Keyword -->
    <meta name="keywords" content="">
    <title>{{ $title }} | Pegasus Trans - international transport company</title>

    <meta property="og:title" content="{{ $title }} | Pegasus Trans - international transport company">
    <meta property="og:url" content="http://pegasus-trans.pl">
    <meta property="og:description" content="Pegasus Trans provides on time and cost efficient road transport for oversize / special transports. We specialise in road transport within the European Union, with a special focus on Germany, France a">
    <meta property="og:type" content="website">
    <meta property="og:image" content="/images/og-image.jpg">
    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/images/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- include the site stylesheet -->
    <link rel="stylesheet" href="{{ mix('/css/style.css') }}">
    @yield('scripts_head')
</head>
<body>
<!-- main container of all the page elements -->
<div id="wrapper">
    @if ($page == 'home')
        @include('partials.header.white')
    @else
        @include('partials.header.default')
    @endif;

    <!-- main of the page -->
    <main id="main">
        @yield('content')
    </main>
    <!-- main of the page end -->

    @include('partials.footer.default')

    <span id="back-top" class="text-center md-round fa fa-angle-up"></span>
    <!-- loader of the page -->
    <div id="loader" class="loader-holder">
        <div class="block"><img src="images/svg/bars.svg" width="60" alt="loader"></div>
    </div>
</div>

@yield('scripts')
<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
