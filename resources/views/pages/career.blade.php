@extends('layout')
@section('content')
    <!-- main slider of the page -->
    <section class="main-slider slider3">
        <!-- slide of the page -->
        <div class="slide bg-full overlay" style="background-image: url(images/img43.jpg);">
            <div class="holder text-center">
                <h2 class="heading text-uppercase">CAREER</h2>
            </div>
        </div>
        <!-- slide of the page end -->
    </section>
    <!-- main slider of the page end -->
    <!-- breadcrumbs of the page -->
    <nav class="breadcrumbs text-center text-uppercase">
        <ul class="list-unstyled">
            <li><a href="/home">Home</a></li>
            <li>/</li>
            <li>CAREER</li>
        </ul>
    </nav>
    <!-- breadcrumbs of the page end -->
    <!-- abt detail of the page -->
    <section class="abt-detail container">
        <div class="row mar display-flex">
            <div class="col-xs-12 col-sm-8 col-md-6">
                <p>
                    We are happy to meet with dedicated and service minded individuals of different experience and qualifications.
                    What matters for us that you treat our customers with great attention and always try to meet highest expectations.
                </p>
                <p>
                    We would also expect from potential team member to be able to co-operate efficiently with other staff members,
                    respect differences and strive to achieve excellence in customer service. We don’t operate under typical linear structure as we believe in equality and strengths of each team member.
                </p>
                <p>
                    Prerequisite for us to meet with you is your ability to speak at least one foreign language at near native level and willingness to progress in a fast paced environment.
                    We would also expect from you to build a strong portfolio of regular and valuable customers.
                </p>
                <p>
                    In order to apply please send us your CV with motivation letter. <br> <br>
                    Our latest jobs: <a href="https://www.olx.pl/oferty/uzytkownik/1cBf1/" target="_blank">click here</a>.
                </p>

            </div>
            <div class="col-xs-12 col-sm-4 col-md-6">
                <div class="img-holder text-center"><img src="images/icon09.jpg" alt="image description" class="img-responsive"></div>
            </div>
        </div>
    </section>
    <!-- abt detail of the page end -->
    <!-- call to action of the page -->
    <section class="call-to-action style3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9">
                    <h2 class="heading">Not sure which solution fits your business needs? Get your shipping quote now</h2>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <a href="/contact" class="btn-primary text-center text-uppercase md-round">Contact us</a>
                </div>
            </div>
        </div>
    </section>
    <!-- call to action of the page end -->
@stop
