@extends('layout')

@section('scripts_head')
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
@stop

@section('content')
    <!-- main slider of the page -->
    <section class="main-slider">
        <!-- slide of the page -->
        <div class="slide bg-full overlay" style="background-image: url(images/img01.jpg);">
            <div class="holder">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1>We provide International Cargo Services</h1>
                            <div class="btn-holder">
                                <a href="/about-us" class="btn-primary text-center text-uppercase active md-round">Find out more</a>
                                <a href="/contact" class="btn-primary text-center text-uppercase md-round">Get a Quote</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slide of the page end -->
        <!-- slide of the page -->
        <div class="slide bg-full overlay" style="background-image: url(images/img14.jpg);">
            <div class="holder">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1>Worldwide Oversize Freight Solutions</h1>
                            <div class="btn-holder">
                                <a href="/about-us" class="btn-primary text-center text-uppercase active md-round">Find out more</a>
                                <a href="/contact" class="btn-primary text-center text-uppercase md-round">Get a Quote</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slide of the page end -->
        <!-- slide of the page -->
        <div class="slide bg-full overlay" style="background-image: url(images/img25.jpg);">
            <div class="holder">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1>Fast and Reliable Transport Services</h1>
                            <div class="btn-holder">
                                <a href="/land-freight" class="btn-primary text-center text-uppercase active md-round">Find out more</a>
                                <a href="/contact" class="btn-primary text-center text-uppercase md-round">Get a Quote</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slide of the page end -->
    </section>
    <!-- main slider of the page end -->

    <!-- Call to action of the page -->
    <section class="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-8">
                    <h2 class="heading heading-m2 text-uppercase">WE'RE READY TO COLLECT YOUR SHIPMENT</h2>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-4">
                    <a href="#request-form" class="btn-primary text-center text-uppercase md-round">Get a quote</a>
                </div>
            </div>
        </div>
    </section>
    <!-- Call to action of the page end -->
    <!-- Counter holder of the page -->
    <div class="counter-holder bg-full" style="background-image: url(images/img09.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 counter-block">
                    <span class="icon"><img src="images/icon04.png" alt="icon" class="img-responsive"></span>
                    <div class="txt-holder">
                        <span class="counter">7900</span>
                        <span class="sub-title">WORLDWIDE TRANSPORT PER YEAR</span>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 counter-block">
                    <span class="icon"><img src="images/icon05.png" alt="icon" class="img-responsive"></span>
                    <div class="txt-holder">
                        <span class="counter">240000</span>
                        <span class="sub-title">TONS OF GOODS MOVED EVERY YEAR</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 counter-block">
                    <span class="icon"><img src="images/icon06.png" alt="icon" class="img-responsive"></span>
                    <div class="txt-holder">
                        <span class="counter">71000000</span>
                        <span class="sub-title">KILOMETERS PER YEAR</span>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 counter-block">
                    <span class="icon"><img src="images/icon07.png" alt="icon" class="img-responsive"></span>
                    <div class="txt-holder">
                        <span class="counter">451</span>
                        <span class="sub-title">CLIENTS WORLDWIDE</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Counter holder of the page end -->
    <!-- Contact sec of the page -->
    <div class="contact-sec container" id="request-form">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <!-- quote block of the page -->
                <div class="quote-block bg-full" style="background-image: url(images/img10.jpg);">
                    <form action="/mailer/request" class="quote-form">
                        <div class="form-loader">
                            <div class="block"><img src="images/svg/bars.svg" width="60" alt="loader"></div>
                        </div>
                        <fieldset>
                            <h2 class="text-uppercase">REQUEST A QUICK QUOTE</h2>
                            <div class="form-group">
                                <div class="col">
                                    <input name="name" type="text" class="form-control md-round" placeholder="Your name">
                                </div>
                                <div class="col">
                                    <input name="email" type="email" class="form-control md-round" placeholder="Your email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col">
                                    <input name="dimension" type="text" class="form-control md-round" placeholder="Dimensions">
                                </div>
                                <div class="col">
                                    <input name="weight" type="text" class="form-control md-round" placeholder="Total Gross Weight (tons)">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col">
                                    <input name="departure" type="text" class="form-control md-round" placeholder="Departure city">
                                </div>
                                <div class="col">
                                    <input name="deliver" type="text" class="form-control md-round" placeholder="Delivery city">
                                </div>
                            </div>
                            <textarea name="message" class="md-round" placeholder="Message"></textarea>
                            <button
                                data-callback="sendForm"
                                data-sitekey="6Ldq5ucZAAAAAB0lYzbG6EWHxk88tsM2ltmZwkLt"
                                data-size="invisible"
                                type="submit" class="btn-primary md-round text-center text-uppercase g-recaptcha">SUBMIT MESSAGE</button>
                        </fieldset>
                        <div class="form-thanks-message">
                            Thank you! Your message has been successfully sent. We will contact you very soon!
                        </div>
                    </form>
                </div>
                <!-- quote block of the page end -->
            </div>
            <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
                <!-- Contact list of the page -->
                <ul class="list-unstyled contact-list">
                    <li>
                        <span class="icon"><i class="ti-alarm-clock"></i></span>
                        <div class="txt-holder">
                            <h3 class="text-uppercase">OPENING HOURS</h3>
                            <time datetime="02-03-2017 20:00">Monday - Friday 08:00 - 16:00</time>
                        </div>
                    </li>
                    <li>
                        <span class="icon"><i class="ti-headphone-alt"></i></span>
                        <div class="txt-holder">
                            <h3 class="text-uppercase">CALL US ANYTIME</h3>
                            <a href="tel:0048721483158" class="tel">0048 721 483 158</a>
                        </div>
                    </li>
                    <li>
                        <span class="icon"><i class="ti-email"></i></span>
                        <div class="txt-holder">
                            <h3 class="text-uppercase">EMAIL US</h3>
                            <a data-mail href="#" class="mail"></a>
                        </div>
                    </li>
                </ul>
                <!-- Contact list of the page end -->
            </div>
        </div>
    </div>
    <!-- Contact sec of the page end -->
    <!-- client sec of the page -->
    <div class="client-sec container">
        <div class="row">
            <div class="col-xs-12 line-box">
                <!-- client logo of the page -->
                <ul class="list-unstyled client-logo line">
                    <li><a href="#"><img src="images/logos/1.svg" alt="logo"></a></li>
                    <li><a href="#"><img src="images/logos/2.svg" alt="logo"></a></li>
                    <li><a href="#"><img src="images/logos/3.svg" alt="logo"></a></li>
                    <li><a href="#"><img src="images/logos/4.svg" alt="logo"></a></li>
                    <li><a href="#"><img src="images/logos/5.svg" alt="logo"></a></li>
                    <li><a href="#"><img src="images/logos/6.svg" alt="logo"></a></li>
                    <li><a href="#"><img src="images/logos/7.svg" alt="logo"></a></li>
                    <li><a href="#"><img src="images/logos/8.svg" alt="logo"></a></li>
                    <li><a href="#"><img src="images/logos/9.svg" alt="logo"></a></li>
                    <li><a href="#"><img src="images/logos/10.svg" alt="logo"></a></li>
                    <li><a href="#"><img src="images/logos/11.svg" alt="logo"></a></li>
                    <li><a href="#"><img src="images/logos/12.svg" alt="logo"></a></li>
                    <li><a href="#"><img src="images/logos/13.svg" alt="logo"></a></li>
                    <li><a href="#"><img src="images/logos/14.svg" alt="logo"></a></li>
                    <li><a href="#"><img src="images/logos/15.svg" alt="logo"></a></li>
                </ul>
                <!-- client logo of the page end -->
            </div>
        </div>
    </div>
    <!-- client sec of the page end -->
@stop
