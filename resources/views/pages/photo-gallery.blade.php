@extends('layout')
@section('content')
    <!-- main slider of the page -->
    <section class="main-slider slider3">
        <!-- slide of the page -->
        <div class="slide bg-full overlay" style="background-image: url(images/img43.jpg);">
            <div class="holder text-center">
                <h2 class="heading text-uppercase">PHOTO GALLERY</h2>
            </div>
        </div>
        <!-- slide of the page end -->
    </section>
    <!-- main slider of the page end -->
    <!-- breadcrumbs of the page -->
    <nav class="breadcrumbs text-center text-uppercase">
        <ul class="list-unstyled">
            <li><a href="/home">Home</a></li>
            <li>/</li>
            <li>PHOTO GALLERY</li>
        </ul>
    </nav>
    <!-- breadcrumbs of the page end -->
    <!-- abt detail of the page -->
    <section class="abt-detail container">
        <div class="row">
            <div class="photo-gallery">
                @for ($i = 1; $i <= 30; $i++)
                    <a href="/images/gallery/{{ $i }}-min.jpg" rel="lightbox" class="photo-gallery__item">
                        <img src="/images/gallery/thumbnails/{{ $i }}-480x450.jpg" alt="">
                    </a>
                @endfor
            </div>
        </div>
    </section>
    <!-- abt detail of the page end -->
    <!-- call to action of the page -->
    <section class="call-to-action style3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9">
                    <h2 class="heading">Not sure which solution fits your business needs? Get your shipping quote now</h2>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <a href="/contact" class="btn-primary text-center text-uppercase md-round">Contact us</a>
                </div>
            </div>
        </div>
    </section>
    <!-- call to action of the page end -->
@stop
