@extends('layout')
@section('content')
    <!-- main slider of the page -->
    <section class="main-slider slider3">
        <!-- slide of the page -->
        <div class="slide bg-full overlay" style="background-image: url(images/img43.jpg);">
            <div class="holder text-center">
                <h2 class="heading text-uppercase">ABOUT US</h2>
            </div>
        </div>
        <!-- slide of the page end -->
    </section>
    <!-- main slider of the page end -->
    <!-- breadcrumbs of the page -->
    <nav class="breadcrumbs text-center text-uppercase">
        <ul class="list-unstyled">
            <li><a href="/home">Home</a></li>
            <li>/</li>
            <li>ABOUT US</li>
        </ul>
    </nav>
    <!-- breadcrumbs of the page end -->
    <!-- abt detail of the page -->
    <section class="abt-detail container">
        <div class="row mar display-flex">
            <div class="col-xs-12 col-sm-8 col-md-6">
                <p>
                    <strong>Pegasus Trans has been established in 2013 to provide on time and cost efficient road transport for oversize / special transports.</strong>
                    <br><br>
                    We specialise in road transport within the European Union, with a special focus on Germany, France and Great Britain.
                    The offer of Pegasus Trans is designed to meet individual customer needs. <br>
                    It is of paramount importance for us that both regular and new customers are fully satisfied with every cent spent on a given freight order.
                </p>
                <p>
                    We operate with the use of the following vehicles: open platform trailers and closed platforms (extendable plane trucks).
                    More information on available modes of transportation can be found in section "Car Fleet". <br>
                    Our motto, is similar to a motto of SAS special forces "Who Dares Wins" - we started in 2013 as a small transport company, offering 3,5to vans,
                    gained strong expertise moving several thousand transports, and in 2016 we started with oversize / special projects.
                </p>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-6">
                <div class="img-holder text-center"><img src="images/icon09.jpg" alt="image description" class="img-responsive"></div>
            </div>
        </div>
        <div class="row display-flex">
            <div class="col-xs-12 col-sm-4 col-md-6">
                <div class="img-holder text-center"><img src="images/icon10.jpg" alt="image description" class="img-responsive"></div>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-6">
                <p>
                    Thanks to a candid dedication to each and every customer and adherence to high quality of services provided we have managed in a relatively short
                    period of time to acquire  both family run transport businesses and strong brands within the European market.
                    <br>
                    "Quality over quantity" is another of our favourites mottos - after several years we realized there is no sense to compete with others offering
                    lower, and lower prices. We provide highest quality and devotion, but in return expect decent, fair remuneration.
                </p>
                <p>
                    This shift in our vision of how we will conduct business proved to be very successful. In recent years we were solid partners for many
                    privately owned companies as well as we provided services to US Army. We were also approved as a partner for NATO. In the upcoming years
                    we plan to move into more complex, special transports.
                </p>
            </div>
        </div>
    </section>
    <!-- abt detail of the page end -->
    <!-- call to action of the page -->
    <section class="call-to-action style3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9">
                    <h2 class="heading">Not sure which solution fits your business needs? Get your shipping quote now</h2>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <a href="/contact" class="btn-primary text-center text-uppercase md-round">Contact us</a>
                </div>
            </div>
        </div>
    </section>
    <!-- call to action of the page end -->
@stop
