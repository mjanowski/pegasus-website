@extends('layout')
@section('content')
    <!-- main slider of the page -->
    <section class="main-slider slider3">
        <!-- slide of the page -->
        <div class="slide bg-full overlay" style="background-image: url(images/img43.jpg);">
            <div class="holder text-center">
                <h2 class="heading text-uppercase">CAR FLEET</h2>
            </div>
        </div>
        <!-- slide of the page end -->
    </section>
    <!-- main slider of the page end -->
    <!-- breadcrumbs of the page -->
    <nav class="breadcrumbs text-center text-uppercase">
        <ul class="list-unstyled">
            <li><a href="/home">Home</a></li>
            <li>/</li>
            <li>CAR FLEET</li>
        </ul>
    </nav>
    <!-- breadcrumbs of the page end -->
    <section class="service-detail container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-4">
                <!-- widget of the page -->
                <div class="widget">
                    <ul class="side-nav list-unstyled">
                        <li><a class="active" href="#flat-trailers">1. Flat trailers</a></li>
                        <li><a href="#semi-trailers">2. Semi trailers</a></li>
                        <li><a href="#low-bed-trailers">3. Low bed trailers</a></li>
                        <li><a href="#container-chassis">4. Container chassis</a></li>
                        <li><a href="#special-platforms-under-plane">5. Special platforms under plane</a></li>
                    </ul>
                </div>
                <!-- widget of the page end -->
            </div>
            <div class="col-xs-12 col-sm-8 col-md-9 col-lg-6 col-lg-offset-1">

                <div id="flat-trailers" class="trailers active">
                    <h1 class="heading">Flat trailers</h1>
                    <div class="carfleet-images">
                        <img src="/images/flat-trailers/1.jpg" alt="" class="carfleet-images__item">
                        <img src="/images/flat-trailers/2.jpg" alt="" class="carfleet-images__item">
                        <img src="/images/flat-trailers/3.jpg" alt="" class="carfleet-images__item">
                    </div>
                </div>

                <div id="semi-trailers" class="trailers">
                    <h1 class="heading">Semi trailers</h1>
                    <div class="carfleet-images">
                        <img src="/images/semi-trailers/1.jpg" alt="" class="carfleet-images__item">
                        <img src="/images/semi-trailers/2.jpg" alt="" class="carfleet-images__item">
                        <img src="/images/semi-trailers/3.jpg" alt="" class="carfleet-images__item">
                        <img src="/images/semi-trailers/4.jpg" alt="" class="carfleet-images__item">
                        <img src="/images/semi-trailers/5.jpg" alt="" class="carfleet-images__item">
                    </div>
                </div>

                <div id="low-bed-trailers" class="trailers">
                    <h1 class="heading">Low bed trailers</h1>
                    <div class="carfleet-images">
                        <img src="/images/low-bed-trailers/1.jpg" alt="" class="carfleet-images__item">
                        <img src="/images/low-bed-trailers/2.jpg" alt="" class="carfleet-images__item">
                        <img src="/images/low-bed-trailers/3.jpg" alt="" class="carfleet-images__item">
                        <img src="/images/low-bed-trailers/4.jpg" alt="" class="carfleet-images__item">
                        <img src="/images/low-bed-trailers/5.jpg" alt="" class="carfleet-images__item">
                    </div>
                </div>
                <div id="container-chassis" class="trailers">
                    <h1 class="heading">Container chassis</h1>
                    <div class="carfleet-images">
                        <img src="/images/container-chassis/1.jpg" alt="" class="carfleet-images__item">
                        <img src="/images/container-chassis/2.jpg" alt="" class="carfleet-images__item">
                    </div>
                </div>
                <div id="special-platforms-under-plane" class="trailers">
                    <h1 class="heading">Special platforms under plane</h1>
                    <div class="carfleet-images">
                        <img src="/images/special-platforms-under-plane/1.jpg" alt="" class="carfleet-images__item">
                        <img src="/images/special-platforms-under-plane/2.jpg" alt="" class="carfleet-images__item">
                        <img src="/images/special-platforms-under-plane/3.jpg" alt="" class="carfleet-images__item">
                        <img src="/images/special-platforms-under-plane/4.jpg" alt="" class="carfleet-images__item">
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- call to action of the page -->
    <section class="call-to-action style3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9">
                    <h2 class="heading">Not sure which solution fits your business needs? Get your shipping quote now</h2>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <a href="/contact" class="btn-primary text-center text-uppercase md-round">Contact us</a>
                </div>
            </div>
        </div>
    </section>
    <!-- call to action of the page end -->
@stop
