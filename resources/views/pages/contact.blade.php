@extends('layout')

@section('scripts_head')
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
@stop

@section('content')
    <!-- main slider of the page -->
    <section class="main-slider slider3">
        <!-- slide of the page -->
        <div class="slide bg-full overlay" style="background-image: url(images/img43.jpg);">
            <div class="holder text-center">
                <h2 class="heading text-uppercase">Contact</h2>
            </div>
        </div>
        <!-- slide of the page end -->
    </section>
    <!-- main slider of the page end -->
    <!-- breadcrumbs of the page -->
    <nav class="breadcrumbs text-center text-uppercase">
        <ul class="list-unstyled">
            <li><a href="/home">Home</a></li>
            <li>/</li>
            <li>Contact</li>
        </ul>
    </nav>
    <!-- breadcrumbs of the page end -->
    <!-- contact sec of the page -->
    <section class="content-sec container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <h3>Contact Info</h3>
                <!-- Contact list of the page -->
                <ul class="list-unstyled contact-list">
                    <li>
                        <span class="icon ti-map-alt"></span>
                        <address>Pegasus Trans Sp. z o. o. <br>Juliusza Kossaka 20, <br>62-800 Kalisz, Poland</address>
                    </li>
                    <li>
                        <span class="icon ti-headphone-alt"></span>
                        <span class="tel">
                            <a href="tel:0048721483158">0048 721 483 158</a>
                        </span>
                    </li>
                    <li>
                        <span class="icon ti-email"></span>
                        <span class="mail">
                            <a href="#" data-mail></a>
                        </span>
                    </li>
                </ul>
                <h3>Opening Hours</h3>
                <!-- Hour list of the page -->
                <ul class="list-unstyled hour-list">
                    <li>Monday – Friday: 08:00 - 16:00</li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-8">
                <h3 class="marg">Get In Touch With Us</h3>
                <p>If you have any question, Please don’t hesitate to send us a message</p>
                <!-- Contact Form of the page -->
                <form action="/mailer/contact" class="contact-form" id="cform">
                    <div class="form-loader">
                        <div class="block"><img src="images/svg/bars.svg" width="60" alt="loader"></div>
                    </div>
                    <fieldset>
                        <div class="form-group">
                            <div class="col">
                                <input name="name" type="text" class="form-control" placeholder="Your Name">
                            </div>
                            <div class="col">
                                <input name="email" type="email" class="form-control" placeholder="Your Email">
                            </div>
                        </div>
                        <textarea name="message" class="form-control" placeholder="Your Message"></textarea>
                        <button
                            data-callback="sendForm"
                            data-sitekey="6Ldq5ucZAAAAAB0lYzbG6EWHxk88tsM2ltmZwkLt"
                            data-size="invisible"
                            type="submit" class="btn-primary text-uppercase text-center md-round g-recaptcha">Send us</button>
                    </fieldset>
                </form>
                <div class="form-thanks-message">
                    Thank you! Your message has been successfully sent. We will contact you very soon!
                </div>
            </div>
        </div>
    </section>
    <!-- contact sec of the page end -->
    <!-- map sec of the page -->
    <section class="map-sec">
        <div class="map" data-lat="51.7904323" data-lng="18.0952107" data-zoom="8">
            <div class="map-info">
                <h2 class="text-uppercase">Pegasus Trans</h2>
                <address>Juliusza Kosaka 20, 62-800 <br>Kalisz, Poland</address>
            </div>
        </div>
    </section>
    <!-- map sec of the page end -->
@stop

@section('scripts')
    <script src="https://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.22&key=AIzaSyDfI624nPMEKE1g3DBAHiiFOD5pb2NB5Sk"></script>
@stop
