New message from request form (pegasus-trans.pl)

Name: {{ $name }}
Email: {{ $email }}
Dimensions: {{ $dimension }}
Total Gross Weight (tons): {{ $weight }}
Departure city: {{ $departure }}
Delivery city: {{ $deliver }}

Message: {{ $message }}
