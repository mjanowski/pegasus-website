@extends('layout')
@section('content')
    <!-- main slider of the page -->
    <section class="main-slider slider3">
        <!-- slide of the page -->
        <div class="slide bg-full overlay" style="background-image: url(images/img43.jpg);">
            <div class="holder text-center">
                <h2 class="heading text-uppercase">404 Page</h2>
            </div>
        </div>
        <!-- slide of the page end -->
    </section>
    <!-- main slider of the page end -->
    <!-- error-page of the page -->
    <section class="error-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2>404</h2>
                    <strong class="txt">Opps! This page Could Not Be Found!</strong>
                    <p>Sorry but the page you are looking for does not exist, have been removed or name changed</p>
                    <a href="/home" class="btn-primary text-center text-uppercase md-round">Back to homepage</a>
                </div>
            </div>
        </div>
    </section>
    <!-- error-page of the page end -->
    <!-- call to action of the page -->
    <section class="call-to-action style2">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <h2 class="heading text-uppercase">WE'RE READY TO COLLECT YOUR SHIPMENT</h2>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <a href="/contact" class="btn-primary text-center text-uppercase md-round">Get a quote</a>
                </div>
            </div>
        </div>
    </section>
    <!-- call to action of the page end -->
@stop
