// page init
$(document).ready(function() {

	"use strict";

	initAddClass();
	// Add Class  init
	function initAddClass() {
		"use strict";

		jQuery('.nav-opener').on( "click", function(e){
			e.preventDefault();
			jQuery("body").toggleClass("nav-active");
		});
	}

	initSlickSlider();
	// Slick Slider init
	function initSlickSlider() {
		"use strict";

		jQuery('.main-slider').slick({
			dots: false,
			speed: 800,
			infinite: true,
			slidesToScroll: 1,
			adaptiveHeight: true,
			autoplay: true,
			arrows: true,
			fade: true,
			autoplaySpeed: 4000
		});
		jQuery('.testimonail-slider').slick({
			dots: true,
			speed: 800,
			infinite: true,
			slidesToScroll: 1,
			adaptiveHeight: true,
			autoplay: true,
			arrows: false,
			fade: true,
			autoplaySpeed: 4000
		});
		jQuery('.testimonail-slider2').slick({
			dots: false,
			speed: 800,
			infinite: true,
			slidesToShow: 2,
			slidesToScroll: 1,
			adaptiveHeight: true,
			autoplay: true,
			arrows: true,
			autoplaySpeed: 4000,
			vertical: true
		});
		jQuery('.blog-slider').slick({
			dots: false,
			speed: 800,
			infinite: true,
			slidesToShow: 4,
			slidesToScroll: 1,
			adaptiveHeight: true,
			autoplay: false,
			arrows: true,
			autoplaySpeed: 4000,
			responsive: [
			{
              breakpoint: 1199,
              settings: {
                slidesToShow: 3
              }
            },
            {
              breakpoint: 1023,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 1
              }
            }
            ]
		});
	}

	initbackTop();
	// backtop init
	function initbackTop() {
		"use strict";

	    var jQuerybackToTop = jQuery("#back-top");
	    jQuery(window).on('scroll', function() {
	        if (jQuery(this).scrollTop() > 100) {
	            jQuerybackToTop.addClass('active');
	        } else {
	            jQuerybackToTop.removeClass('active');
	        }
	    });
	    jQuerybackToTop.on('click', function(e) {
	        jQuery("html, body").animate({scrollTop: 0}, 900);
	    });
	}

	initStyleChanger();
	// style changer
	function initStyleChanger() {
		"use strict";

		var element = jQuery('#style-changer');

		if(element) {
			$.ajax({
				url: element.attr('data-src'),
				type: 'get',
				dataType: 'text',
				success: function(data) {
					var newContent = jQuery('<div>', {
						html: data
					});

					newContent.appendTo(element);
				}
			});
		}
	}

    initStickyHeader();
	// sticky header init
	function initStickyHeader() {

		"use strict";

		var win = jQuery(window),
			stickyClass = 'sticky';

		jQuery('#header').each(function() {
			var header = jQuery(this);
			var headerOffset = header.offset().top || 0;
			var flag = true;


			jQuery(this).css('height' , jQuery(this).innerHeight());

			function scrollHandler() {
				if (win.scrollTop() > headerOffset) {
					if (flag){
						flag = false;
						header.addClass(stickyClass);
					}
				} else {
					if (!flag) {
						flag = true;
						header.removeClass(stickyClass);
					}
				}
			}

			scrollHandler();
			win.on('scroll resize orientationchange', scrollHandler);
		});
	}

	initVegasSlider();
	// Vegas Slider init
	function initVegasSlider() {
	  jQuery("#bgvid").vegas({
	      slides: [
	        {   src: 'images/img22.jpg',
	            video: {
	                src: [
	                    'video/polina.webm',
	                    'video/polina.mp4'
	                ],
	                loop: true,
	                timer: false,
	                mute: true
	            }
	        }
	    ]
	  });
	}

	initMarquee();
	// running line init
	function initMarquee() {
		"use strict";

		jQuery('.line-box').marquee({
			line: '.line',
			animSpeed: 50
		});
	}

	initProgressBar();
	// Progress Bar init
	function initProgressBar() {
		if( jQuery(".progress-bar").length != '' ){
			var waypoint = new Waypoint({
				element: document.getElementById('progress-bar'),
				handler: function(direction) {
					console.log('Scrolled to waypoint!');
					jQuery('.progress-bar li').each(function() {
							var widthBar = jQuery(this).find('.over').attr('data-percent');
							jQuery(this).find('.over').animate({
								'width': widthBar
							});
					});
				},
				offset: '80%'
			});
		}
	}

	initLightbox();
	// modal popup init
	function initLightbox() {

		"use strict";

		jQuery('a.lightbox, a[rel*="lightbox"]').fancybox({
			padding: 0
		});
		jQuery("#newsletter-hiddenlink").fancybox().trigger('click');
	}

	initAccordion();
	// accordion menu init
	function initAccordion() {
		"use strict";

		jQuery('ul.accordion').slideAccordion({
			addClassBeforeAnimation: true,
			opener: 'a.opener',
			slider: 'div.slide',
			animSpeed: 300
		});
	}

	initGoogleMap();
	// GoogleMap init
	function initGoogleMap() {
		"use strict";

		jQuery('.map').googleMapAPI({
			marker: 'images/icon.png',
			mapInfoContent: '.map-info',
			streetViewControl: false,
			mapTypeControl: false,
			scrollwheel: false,
			panControl: false,
			zoomControl: false
		});
	}

	initCounter();
	// Counter init
	function initCounter() {

		"use strict";

		jQuery('.counter').counterUp({
			delay: 20,
			time: 1000
		});
	}

    initDataMail();
    // Counter init
    function initDataMail() {

        "use strict";

        jQuery('[data-mail]').attr('href', 'mailto:info@pegasus-trans.pl');
        jQuery('[data-mail]').html('info@pegasus-trans.pl');
    }



    initStartHero();

    function initStartHero() {
        $('.start-hero__images img').each(function(){
            if ($(window).width() >= 1024) {
                $(this).attr('src', $(this).data('src'));
            }
        })
        $('.start-hero__links a').hover(function(){
            var rel = $(this).attr('data-rel');
            $(rel).toggleClass('active');
        });
    }

});

jQuery(window).on('load', function() {

	"use strict";

	initPreLoader()
	// PreLoader init
	function initPreLoader() {
	    "use strict";

	    jQuery('#loader').delay(500).fadeOut();
	}

    initGalleryMasonry();

    function initGalleryMasonry() {
        var elem = document.querySelector('.photo-gallery');
        if (elem) {
            var msnry = new Masonry( elem, {
                // options
                itemSelector: '.photo-gallery__item',
                // columnWidth: 200
            });
        }
    }

    initTrailers();

    function initTrailers() {
        $('.side-nav a').click(function(e){
            e.preventDefault();
            var target = $(this).attr('href');
            console.log(target);
            $('.side-nav a').removeClass('active');
            $(this).addClass('active');
            $('.trailers.active').fadeOut(500, function(){
                $('.trailers.active').removeClass('active');
                $(target).fadeIn(500, function(){
                    $(target).addClass('active');
                })
            })

        })
    }
});

$('form').submit(function(e){
    e.preventDefault();
});

function sendForm() {
    var form = $('form')
    $('.has-error').removeClass('has-error')
    form.addClass('proceeding');
    $('.form-thanks-message').removeClass('show');
    $('.error-message').remove();

    $.ajax({
        url: form.attr('action'),
        data: form.serialize(),
        type: "post",
        dataType: "json",
        success: function (data, textStatus, xhr) {
            $('form')[0].reset();
            $('.form-thanks-message').addClass('show');
            setTimeout(function() {
                $(".form-thanks-message").slideUp(400, function(){
                    $(this).removeClass('show')
                });
            }, 3000);
            $('form').removeClass('proceeding');
        },
        error: function(xhr) {
            $.each(xhr.responseJSON, function(index, value){
                $('[name="'+ index +'"]').addClass('has-error');
                $('[name="'+ index +'"]').after('<span class="error-message help-block">'+ value +'</span>')
            })
            $('form').removeClass('proceeding');
            $('form').find('button[type=submit]').removeClass('disabled');
        }
    });
}
