<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SiteController extends Controller
{
    /**
     * Available page slugs
     */
    const AVAILABLE_PAGES = [
            'start' => 'Pegasus',
            'home' => 'Home',
            'about-us' => 'About us',
            'land-freight' => 'Car fleet',
            'photo-gallery' => 'Photo gallery',
            'career' => 'Career',
            'contact' => 'Contact'
        ];

    /**
     * Renders a page
     *
     * @param string $page
     */
    public function indexAction(string $page = 'start')
    {
        if (!array_key_exists($page, self::AVAILABLE_PAGES)) {
            throw new NotFoundHttpException();
        }
        return view('pages.' . $page,
            [
                'title' => self::AVAILABLE_PAGES[$page],
                'page' => $page
            ]
        );
    }
}
