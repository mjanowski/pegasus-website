<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{

    public function requestAction(Request $request)
    {
        $fields = $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'dimension' => 'required',
            'weight' => 'required',
            'departure' => 'required',
            'deliver' => 'required',
        ]);

        if (!$this->validateGoogleCaptcha($request->get('g-recaptcha-response'))) {
            return response()->json(['g-recaptcha-response' => 'Captcha error'], 403);
        }
        Mail::raw(view('emails.request', $fields), function($msg){
            $msg->to([env('ADMIN_MAIL')])->subject('Request Form');
        });

        return response()->json(['message' => 'Messsage sent'], 200);
    }

    public function contactAction(Request $request)
    {
        $fields = $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ]);

        if (!$this->validateGoogleCaptcha($request->get('g-recaptcha-response'))) {
            return response()->json(['g-recaptcha-response' => 'Captcha error'], 403);
        }
        Mail::raw(view('emails.contact', $fields), function($msg){
            $msg->to([env('ADMIN_MAIL')])->subject('Contact Form');
        });

        return response()->json(['message' => 'Messsage sent'], 200);
    }

    /**
     * @param $value
     * @return mixed
     */
    protected function validateGoogleCaptcha($value)
    {
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . env('GCAPTCHA_KEY') .  '&response=' . urlencode($value);
        $response = file_get_contents($url);
        $responseKeys = json_decode($response,true);
        return $responseKeys["success"];
    }

}
