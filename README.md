# Pegasus website 

Source code of pegasus website based on Lumen framework.
## Prerequirements

To build & run this project you have to make sure that you have `Docker` and `Docker Compose` installed.

### Create .env file
Before you start please create .env file based on .env.example and provide the necessary variables.

`cp .env.example .env`

## Build & run containers
Build image `docker-compose build` 

Run containers in detached mode `docker-compose up -d`

### Edit templates & assets
Path to templates:
`resources/views/`

Path to assets 
`resources/assets/`
### Compile assets
```
npm run dev - for development purposes
npm run watch - to watch file changes
npm run production - for production purposes
```

